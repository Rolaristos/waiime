import { React, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';
import { Carousel, Modal } from 'react-bootstrap';

export default function MainContent() {
  const [showModal, setShowModal] = useState(false);
  const [selectedImage, setSelectedImage] = useState('');

  const openModal = (imageSrc) => {
    const imageUrl = imageSrc.replace(/(\d+)x(\d+)/, '1000x1000');
    setSelectedImage(imageUrl);
    setShowModal(true);
  };

  const closeModal = () => {
    setSelectedImage('');
    setShowModal(false);
  };

  return (
    <motion.div
      className="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column"
      initial={{ transform: 'translateX(-200vw)' }}
      animate={{ transform: 'translateX(0vw)' }}
      exit={{ transform: 'translateX(100vw)', opacity: 0, transition: { duration: 0.5 } }}
      transition={{ duration: 1, delay: 0.3 }}
    >
      <header data-bs-theme="dark">
        <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
          <div className="container-fluid">
            <Link className="navbar-brand" to="/">
              <img src="/waiime.png" alt="Waiime Logo" style={{ height: '40px', marginRight: '10px' }} />Waiime
            </Link>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarCollapse">
              <ul className="navbar-nav me-auto mb-2 mb-md-0">
                <li className="nav-item">
                  <a className="nav-link active" aria-current="page" href="#">Home</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>

      <main>

        <Carousel interval={null} wrap={false}>
          <Carousel.Item>
            <img className="bd-placeholder-img" width="100%" height="950" src="/img/home-banner1.png" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
            <Carousel.Caption>
              <h1>Founded in 2022</h1>
              <p className="text-black">
                by a group of friends passionate<br />
                about the idea of spreading happiness through plush toys,<br />
                Waiime quickly emerged as a leader in the plush toy industry.
              </p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img className="bd-placeholder-img" width="100%" height="950" src="/img/home-banner2.png" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
            <Carousel.Caption>
              <h1>Our beginnings</h1>
              <p className="text-black bg-white">
                From our humble beginnings in a small artisan workshop,<br />
                we have grown to become a brand known for the outstanding quality<br />
                and originality of our plush toys.
              </p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img className="bd-placeholder-img" width="100%" height="950" src="/img/home-banner3.png" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
            <Carousel.Caption>
              <h1>Discover our creations</h1>
              <p className="text-black">
                Each creation from Waiime tells a unique story,<br />
                crafted with love and attention to detail to capture the imagination<br />
                and spread joy among both young and old.
              </p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>

        <div className="container marketing">

          <div className="row">
            <h1 className='mt-5' id='summary'>Table of Contents</h1>
            <div className="col-lg-4">
              <svg className="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="var(--bs-secondary-color)" /></svg>
              <h2 className="fw-normal">Our mission</h2>
              <p>Explore the heart and soul of Waiime by discovering our mission. Learn how we're dedicated to spreading joy, fostering emotional connections, and making a difference in the world through our plush toys. Click below for more insights into our mission.</p>
              <p><a className="btn btn-secondary" href="#mission">View details &raquo;</a></p>
            </div>
            <div className="col-lg-4">
              <svg className="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="var(--bs-secondary-color)" /></svg>
              <h2 className="fw-normal">Our products</h2>
              <p>Dive into the enchanting world of Waiime as we unveil our delightful range of plush toys. From classic teddy bears to whimsical characters, explore the creativity and craftsmanship behind each cuddly creation. Click below to discover more about our captivating products.</p>
              <p><a className="btn btn-secondary" href="#prod">View details &raquo;</a></p>
            </div>
            <div className="col-lg-4">
              <svg className="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="var(--bs-secondary-color)" /></svg>
              <h2 className="fw-normal">Code of conduct</h2>
              <p>Discover the ethical foundation that guides every aspect of Waiime's operations. Learn about our commitment to integrity, quality, and social responsibility through our comprehensive code of conduct. Click below to explore the principles that define our company's values.</p>
              <p><a className="btn btn-secondary" href="#coc">View details &raquo;</a></p>
            </div>
            <div className="col-lg-4">
              <svg className="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="var(--bs-secondary-color)" /></svg>
              <h2 className="fw-normal">Organisation of Waiime</h2>
              <p>Peek behind the scenes and uncover the inner workings of Waiime's organizational structure. Explore how our collaborative team drives innovation, creativity, and excellence in every aspect of our operations. Click below to learn more about the dynamic organization that powers Waiime.</p>
              <p><button className="btn btn-secondary" onClick={() => openModal('/img/hierarchy.png')}>View details &raquo;</button></p>
            </div>
            <div className="col-lg-4">
              <svg className="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="var(--bs-secondary-color)" /></svg>
              <h2 className="fw-normal">Future direction</h2>
              <p>Explore the heart and soul of Waiime by discovering our mission. Learn how we're dedicated to spreading joy, fostering emotional connections, and making a difference in the world through our plush toys. Click below for more insights into our mission.</p>
              <p><a className="btn btn-secondary" href="#future">View details &raquo;</a></p>
            </div>
            <div className="col-lg-4">
              <svg className="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="var(--bs-secondary-color)" /></svg>
              <h2 className="fw-normal">Question & Answer</h2>
              <p>Explore the heart and soul of Waiime by discovering our mission. Learn how we're dedicated to spreading joy, fostering emotional connections, and making a difference in the world through our plush toys. Click below for more insights into our mission.</p>
              <p><button className="btn btn-secondary" onClick={() => openModal('/img/q&a.png')}>View details &raquo;</button></p>
            </div>
          </div>

          <Modal show={showModal} onHide={closeModal} style={{ "--bs-modal-width": "932px" }}>
            <Modal.Body className="text-center" style={{ minWidth: '900px', minHeight: '900px' }}>
              <img src={selectedImage} alt="Enlarged image" className="img-fluid" style={{ maxWidth: '900px', maxHeight: '900px' }} />
            </Modal.Body>
          </Modal>

          <hr className="featurette-divider" />

          <div className="row featurette">
            <div className="col-md-7" id="mission">
              <h2 className="featurette-heading fw-normal lh-1">Our mission</h2>
              <p className="lead">Our mission at Waiime is simple: to awaken the magic of childhood and maintain emotional bonds through our plush toys. We firmly believe that these cuddly companions are more than just toys; they are sources of comfort, inspiration, and companionship for those who adopt them.</p>
              <p className='lead'><br />Our dedication to this mission extends far beyond just making toys. We aim to create moments of joy and connection between children and their plush companions, fostering their imagination and unforgettable memories. At Waiime, each plush is meticulously crafted with love and care to embody these core values.</p>
            </div>
            <div className="col-md-5">
              <Carousel style={{ width: '500px', height: '500px' }}>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/ourmission1.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                  <Carousel.Caption>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/ourmission2.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                  <Carousel.Caption>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/ourmission3.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                  <Carousel.Caption>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/ourmission5.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                  <Carousel.Caption>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/ourmission4.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                  <Carousel.Caption>
                  </Carousel.Caption>
                </Carousel.Item>
              </Carousel>
            </div>
          </div>

          <hr className="featurette-divider" />

          <div className="row featurette" id="prod">
            <div className="col-md-7 order-md-2">
              <h2 className="featurette-heading fw-normal lh-1">Our products</h2>
              <ul className='text-left'>
                <br /><li>
                  <strong>Plush Toys</strong><br />
                  <ul>
                    <li>Sizes: Variety of sizes available, ranging from small pocket-sized plush toys to large cuddly plush toys</li>
                    <li>Selection: Wide selection of adorable characters, from classic animals to fantastical creatures</li>
                    <li>Materials: Made from high-quality fabrics such as soft cotton and plush fleece for a comfortable feel</li>
                    <li>Stuffing: Fluffy and durable polyester, providing a long-lasting plush feel</li>
                    <li>Detailing: Detailed with meticulous embroidery for an attractive appearance</li>
                    <li>Safety: Designed with non-toxic materials, compliant with the strictest safety standards</li>
                  </ul>
                </li>
                <br /><li>
                  <strong>Shipping Packaging</strong><br />
                  <ul>
                    <li>Materials: Use of eco-friendly and recyclable packaging materials to reduce our environmental impact</li>
                    <li>Protection: Each plush toy is carefully wrapped in tissue paper to protect against friction and damage during transportation</li>
                    <li>Tracking: Includes a clear shipping label for fast delivery</li>
                  </ul>
                </li>
              </ul>
            </div>
            <div className="col-md-5 mx-auto">
              <Carousel style={{ width: '500px', height: '500px' }}>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/products1.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                  <Carousel.Caption>
                    <h1>by Mewaii X Waiime</h1>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/products2.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                  <Carousel.Caption>
                    <h1>by Mewaii X Waiime</h1>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/products3.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                  <Carousel.Caption>
                    <h1>by Mewaii X Waiime</h1>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/products4.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                  <Carousel.Caption>
                    <h1>By Waiime</h1>
                  </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/products5.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                  <Carousel.Caption>
                    <h1>by Mewaii X Waiime</h1>
                  </Carousel.Caption>
                </Carousel.Item>
              </Carousel>
            </div>
          </div>

          <hr className="featurette-divider" />

          <div className="row featurette">
            <div className="col-md-7" id="coc">
              <h2 className="featurette-heading fw-normal lh-1">Code of conduct</h2>
              <div className="mb-3 lh-lg">
                <p className='d-flex text-align-bottom'>
                  <h1>R</h1><strong>esponsability</strong> : We act with accountability and integrity.
                </p>
                <p className='d-flex text-align-bottom'>
                  <h1>E</h1><strong>mpathy</strong> : We listen and understand others' perspectives.
                </p>
                <p className='d-flex text-align-bottom'>
                  <h1>S</h1><strong>afety</strong> : We prioritize a safe and secure environment.
                </p>
                <p className='d-flex text-align-bottom'>
                  <h1>P</h1><strong>ositive Attitude</strong> : We foster optimism and encouragement.
                </p>
                <p className='d-flex text-align-bottom'>
                  <h1>E</h1><strong>xcellence</strong> : We strive for quality and continuous improvement.
                </p>
                <p className='d-flex text-align-bottom'>
                  <h1>C</h1><strong>aring</strong> : We show compassion and support for one another.
                </p>
                <p className='d-flex text-align-bottom'>
                  <h1>T</h1><strong>eamwork</strong> : We collaborate to achieve common goals.
                </p>
              </div>
            </div>
            <div className="col-md-5">
              <Carousel style={{ width: '500px', height: '500px' }}>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/coc3.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/coc2.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/coc1.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/coc4.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                </Carousel.Item>
              </Carousel>
            </div>
          </div>
          <a href="#summary">Back to the table of Contents</a>

          <hr className="featurette-divider" />

          <hr className="featurette-divider" />

          <div className="row featurette">
            <div className="col-md-7 order-md-2" id="future">
              <h2 className="featurette-heading fw-normal lh-1">Future direction</h2>
              <p className="lead">
                Our future direction:
                In terms of our future direction, Waiime aims to explore new opportunities for growth and innovation in the plush toy industry.
              </p>
              <p className="lead">
                Waiime is already working on a new project called "draw, and we make," which involves sending your children's drawings to our web page, and the 10 with the most votes will be manufactured. This will enable 10 children to see their dream become a reality, and others to try their luck again.
              </p>
              <p className="lead">
                We are committed to expanding our product range, strengthening our strategic partnerships, and continuing to inspire the imaginations of children around the world.
              </p>
            </div>

            <div className="col-md-5 mx-auto">
              <Carousel style={{ width: '500px', height: '500px' }}>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/future1.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/future2.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/future3.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/future4.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/future5.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/future6.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/future7.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                </Carousel.Item>
                <Carousel.Item>
                  <img className="bd-placeholder-img" width="100%" height="100%" src="/img/future8.webp" aria-hidden="true" preserveAspectRatio="xMidYMid slice" focusable="false"></img>
                </Carousel.Item>
              </Carousel>
            </div>
          </div>
          <a href="#summary">Back to the table of Contents</a>

          <hr className="featurette-divider" />

        </div>

        <footer className="container">
          <p className="float-end"><a href="#">Back to top</a></p>
          <p>&copy; 2022–2024 Waiime Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
        </footer>
      </main>
    </motion.div>
  );
}