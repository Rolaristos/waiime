import React from 'react';
import { Route, Routes, useLocation } from 'react-router-dom';
import { AnimatePresence } from 'framer-motion';

// On import toutes les components qui seront utilisées dans les routes
import Home from './pages/Home';
import MainContent from './pages/MainContent';

function AnimatedRoutes() {
    const location = useLocation();

    return (
        <AnimatePresence>
            <Routes location={location} key={location.pathname}>
                {/* Route pour la page d'accueil */}
                <Route path="/" element={<Home />} />
                <Route path="/waiime" element={<MainContent />} />
            </Routes>
        </AnimatePresence>
    )
}

export default AnimatedRoutes;