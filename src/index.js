import React from 'react';
import ReactDOM from 'react-dom/client'; // Import de ReactDOM
import './index.css';
import App from './App';

// Création d'une racine React pour le rendu
const root = ReactDOM.createRoot(document.getElementById('root'));
// Rendu de l'application dans la racine avec le strict mode de React et le contexte utilisateur
root.render(
  <React.StrictMode>
      <App />
  </React.StrictMode>
);
